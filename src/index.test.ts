import { getJunitXml } from './index';
import { TestSuiteReport } from './TestResults';

it('should correctly serialise an empty report', () => {
  const minimalReport: TestSuiteReport = {
    suites: [],
  };

  expect(getJunitXml(minimalReport)).toMatchSnapshot();
});

it('should correctly serialise a minimal report', () => {
  const minimalReport: TestSuiteReport = {
    suites: [
      {
        testCases: [
          { name: 'Successful test' },
        ],
      },
    ],
  };

  expect(getJunitXml(minimalReport)).toMatchSnapshot();
});

it('should correctly serialise a maximally detailed report', () => {
  const maximalReport: TestSuiteReport = {
    name: 'Some test suite report name',
    time: 4.2,
    suites: [
      {
        name: 'Some suite',
        timestamp: new Date(Date.UTC(1989, 10, 3)),
        hostname: 'some-hostname',
        time: 1.1337,
        testCases: [
          {
            name: 'Successful test',
            assertions: 2,
            classname: 'successful-test-class',
            time: 0.72,
          },
          {
            name: 'Skipped test',
            assertions: 2,
            skipped: true,
          },
          {
            name: 'Unskipped test',
            skipped: false,
          },
          {
            name: 'Failing test',
            failures: [
              { message: 'First failure', type: 'some-type' },
              { message: 'Second failure' },
            ],
          },
          {
            name: 'Another failing test',
            failures: [
              { message: 'Just one failure' },
            ],
          },
          {
            name: 'Erroring test',
            errors: [
              { message: 'First error', type: 'some-type' },
              { message: 'Second error' },
            ],
          },
          {
            name: 'Another erroring test',
            errors: [
              { message: 'Just one error' },
            ],
          },
          {
            name: 'Test with output',
            systemOut: [
              'First output',
              'Second output',
            ],
          },
          {
            name: 'Test with single output',
            systemOut: [
              'Only output',
            ],
          },
          {
            name: 'Test with error output',
            systemErr: [
              'First error output',
              'Second error output',
            ],
          },
          {
            name: 'Test with single error output',
            systemErr: [
              'Only error output',
            ],
          },
        ],
      },
      {
        name: 'Suite without test cases',
        testCases: [],
      },
      {
        name: 'Another suite',
        testCases: [
          { name: 'Some successful test' },
        ],
      },
    ],
  };

  expect(getJunitXml(maximalReport)).toMatchSnapshot();
});
