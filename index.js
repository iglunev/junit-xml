"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var xml_1 = __importDefault(require("xml"));
function getJunitXml(report) {
    var testSuites = report.suites.map(attachTestSuiteMetadata);
    var xmlObject = {
        testsuites: testSuites.map(function (testSuite) { return ({ testsuite: getTestSuiteXml(testSuite) }); }),
    };
    var testsuitesAttributes = {
        tests: testSuites.reduce(function (tests, suite) { return tests + suite.tests; }, 0).toString(),
        errors: testSuites.reduce(function (errors, suite) { return errors + suite.errors; }, 0).toString(),
        failures: testSuites.reduce(function (failures, suite) { return failures + suite.failures; }, 0).toString(),
        skipped: testSuites.reduce(function (skipped, suite) { return skipped + suite.skipped; }, 0).toString(),
    };
    if (report.name) {
        testsuitesAttributes.name = report.name;
    }
    if (report.time) {
        testsuitesAttributes.time = report.time.toString();
    }
    xmlObject.testsuites.push({ _attr: testsuitesAttributes });
    return xml_1.default(xmlObject);
}
exports.getJunitXml = getJunitXml;
function getTestSuiteXml(testSuite) {
    var testSuiteXmlObject = testSuite.testCases
        .map(function (testCase) { return ({ testcase: getTestCaseXml(testCase) }); });
    var testsuiteAttributes = {
        tests: testSuite.tests.toString(),
        errors: testSuite.errors.toString(),
        failures: testSuite.failures.toString(),
        skipped: testSuite.skipped.toString(),
    };
    if (testSuite.name) {
        testsuiteAttributes.name = testSuite.name;
    }
    if (testSuite.hostname) {
        testsuiteAttributes.hostname = testSuite.hostname;
    }
    if (testSuite.time) {
        testsuiteAttributes.time = testSuite.time.toString();
    }
    if (testSuite.timestamp) {
        testsuiteAttributes.timestamp = testSuite.timestamp.toISOString();
    }
    testSuiteXmlObject.push({ _attr: testsuiteAttributes });
    return testSuiteXmlObject;
}
function getTestCaseXml(testCase) {
    var testCaseXmlObject = [];
    var testcaseAttributes = {
        name: testCase.name,
        classname: testCase.classname || testCase.name,
    };
    if (testCase.time) {
        testcaseAttributes.time = testCase.time.toString();
    }
    if (testCase.assertions) {
        testcaseAttributes.assertions = testCase.assertions.toString();
    }
    testCaseXmlObject.push({ _attr: testcaseAttributes });
    if (testCase.skipped) {
        testCaseXmlObject.push({ skipped: '' });
    }
    var errors = (Array.isArray(testCase.errors))
        ? testCase.errors.map(function (error) { return ({ error: formatMessage(error) }); })
        : [];
    var failures = (Array.isArray(testCase.failures))
        ? testCase.failures.map(function (failure) { return ({ error: formatMessage(failure) }); })
        : [];
    var systemOuts = (Array.isArray(testCase.systemOut))
        ? testCase.systemOut.map(function (systemOut) { return ({ 'system-out': systemOut }); })
        : [];
    var systemErrs = (Array.isArray(testCase.systemErr))
        ? testCase.systemErr.map(function (systemErr) { return ({ 'system-err': systemErr }); })
        : [];
    return testCaseXmlObject.concat(errors).concat(failures).concat(systemOuts).concat(systemErrs);
}
;
function attachTestSuiteMetadata(testSuite) {
    return __assign({}, testSuite, { tests: testSuite.testCases.length, failures: testSuite.testCases.filter(isFailing).length, errors: testSuite.testCases.filter(hasErrors).length, skipped: testSuite.testCases.filter(isSkipped).length });
}
function isSkipped(testCase) {
    return testCase.skipped === true;
}
function isFailing(testCase) {
    return Array.isArray(testCase.failures) && testCase.failures.length > 0;
}
function hasErrors(testCase) {
    return Array.isArray(testCase.errors) && testCase.errors.length > 0;
}
function formatMessage(message) {
    if (typeof message.type === 'string') {
        return [message.message, { _attr: { type: message.type } }];
    }
    return message.message;
}
