import { TestSuiteReport } from './TestResults';
export { TestSuiteReport } from './TestResults';
export declare function getJunitXml(report: TestSuiteReport): string;
