image: node:10

stages:
  - build
  - test
  - release
  - confidenceCheck

build:
  stage: build
  script:
    - wipCommits=`git log --grep=^WIP`; if [ -n "$wipCommits" ] && [ "$CI_COMMIT_REF_NAME" = "master" ]; then echo "WIP commits detected in `master`; failing build."; exit 1; fi
    - yarn install --frozen-lockfile
    - yarn run build
  artifacts:
    expire_in: 1 month
    paths:
      - node_modules
      - ./*.d.ts
      - ./*.js
      - ./**/*.d.ts
      - ./**/*.js

test:
  stage: test
  script:
    - wipCommits=`git log --grep=^WIP`; if [ -n "$wipCommits" ] && [ "$CI_COMMIT_REF_NAME" != "master" ]; then echo "WIP commits detected; skipping tests."; exit 0; fi
    - yarn test --ci --reporters=default --reporters=jest-junit
  artifacts:
    expire_in: 1 week
    paths:
      - coverage
    reports:
      junit: junit.xml

release_snapshot:
  stage: release
  dependencies:
    - build
  only:
    - branches
  script:
    - apt-get update
    - apt-get install --assume-yes jq
    - echo "//registry.npmjs.org/:_authToken=$NPM_TOKEN" > ~/.npmrc
    - VERSION="$(cat package.json | jq -r '.version')"
    - NEWVERSION="$VERSION-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA"
    - sed --in-place --expression="s/\"$VERSION\"/\"$NEWVERSION\"/" package.json
#    - if [ $CI_COMMIT_REF_SLUG == "master" ]; then export TAGNAME="next"; else export TAGNAME="$CI_COMMIT_REF_SLUG"; fi
#    - npm publish --tag "$TAGNAME"
    - npm pack
    - mv "$CI_PROJECT_NAME-$NEWVERSION.tgz" package.tgz
    - echo -e "To install this version of $CI_PROJECT_NAME, run:\n    yarn add --dev $CI_PROJECT_NAME@$TAGNAME"
  environment:
    name: snapshot/$CI_COMMIT_REF_SLUG
    on_stop: unlist_snapshot
  artifacts:
    expire_in: 1 month
    paths:
      - package.tgz
unlist_snapshot:
  stage: release
  when: manual
  only:
    - branches
  script:
    - echo "//registry.npmjs.org/:_authToken=$NPM_TOKEN" > ~/.npmrc
    - if [ $CI_COMMIT_REF_SLUG == "master" ]; then export TAGNAME="next"; else export TAGNAME="$CI_COMMIT_REF_SLUG"; fi
    - npm dist-tag rm "$CI_PROJECT_NAME" "$TAGNAME"
  environment:
    name: snapshot/$CI_COMMIT_REF_SLUG
    action: stop

release:
  stage: release
  dependencies:
    - build
  only:
    - tags
  script:
    - echo "//registry.npmjs.org/:_authToken=$NPM_TOKEN" > ~/.npmrc
#    - npm publish
    - npm pack
    # $CI_COMMIT_REF_NAME is the tag, which should be in the form `v<version>`.
    # Thus, if we strip the `v`, we can get the package name.
    - mv "$CI_PROJECT_NAME-${CI_COMMIT_REF_NAME:1}.tgz" package.tgz
  environment:
    name: production
    url: https://www.npmjs.com/package/$CI_PROJECT_NAME

# See https://gitlab.com/help/user/project/merge_requests/code_quality.md
code_quality:
  stage: confidenceCheck
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env SOURCE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/codequality:$SP_VERSION" /code
  artifacts:
    reports:
      codequality: gl-code-quality-report.json

# See https://gitlab.com/help/user/project/merge_requests/sast.md
sast:
  stage: confidenceCheck
  dependencies:
    - build
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env SAST_CONFIDENCE_LEVEL="${SAST_CONFIDENCE_LEVEL:-3}"
        --volume "$PWD:/code"
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/sast:$SP_VERSION" /app/bin/run /code
  artifacts:
    reports:
      sast: gl-sast-report.json

# See https://gitlab.com/help/user/project/merge_requests/dependency_scanning.md
dependency_scanning:
  stage: confidenceCheck
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env DEP_SCAN_DISABLE_REMOTE_CHECKS="${DEP_SCAN_DISABLE_REMOTE_CHECKS:-false}"
        --volume "$PWD:/code"
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/dependency-scanning:$SP_VERSION" /code
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json

# See https://gitlab.com/help/user/project/merge_requests/license_management.md
license_management:
  stage: confidenceCheck
  image:
    name: "registry.gitlab.com/gitlab-org/security-products/license-management:$CI_SERVER_VERSION_MAJOR-$CI_SERVER_VERSION_MINOR-stable"
    entrypoint: [""]
  allow_failure: true
  script:
    - /run.sh analyze .
  artifacts:
    reports:
      license_management: gl-license-management-report.json
